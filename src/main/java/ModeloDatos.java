import models.Jugador;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;

public class ModeloDatos {

    private Connection con;
    private Statement set;
    private ResultSet rs;

    private static final Logger logger = LoggerFactory.getLogger(ModeloDatos.class);

    public void abrirConexion() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Con variables de entorno
            String dbUser = System.getenv().get("DATABASE_USER");
            String dbPass = System.getenv().get("DATABASE_PASS");

            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/baloncesto", dbUser, dbPass);



        } catch (Exception e) {
            // No se ha conectado
            logger.info("No se ha podido conectar");
            logger.debug(e.getMessage());
        }
    }

    public boolean existeJugador(String nombre) {
        boolean existe = false;
        String cad;
        try {
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                cad = rs.getString("Nombre");
                cad = cad.trim();
                if (cad.compareTo(nombre.trim()) == 0) {
                    existe = true;
                }
            }
            rs.close();
            set.close();
        } catch (Exception e) {
            // No lee de la tabla
            logger.info("No lee de la tabla");
            logger.debug(e.getMessage());
        }
        return (existe);
    }

    public void actualizarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores SET votos=votos+1 WHERE nombre " + " LIKE '%" + nombre + "%'");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No modifica la tabla
            logger.info("No modifica la tabla");
            logger.debug(e.getMessage());
        }
    }

    public void insertarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("INSERT INTO Jugadores " + " (nombre,votos) VALUES ('" + nombre + "',1)");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No inserta en la tabla
            logger.info("No inserta en la tabla");
            logger.debug(e.getMessage());
        }
    }

    public void reinicializarVotos(){
        try {
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores SET votos=0 WHERE id > 0 ");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No modifica la tabla
            logger.info("No Reinicializa los votos");
            logger.debug(e.getMessage());
        }
    }

    //Devuelve todos los jugadores de la base de datos en un ArrayList
    public ArrayList<Jugador> getAllJugadores() {

        ArrayList<Jugador> listaJugadores = new ArrayList<>();

        try {
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                listaJugadores.add(new Jugador(
                        rs.getString("nombre"),
                        rs.getInt("votos")
                ));
            }

            set.close();
        } catch (SQLException e) {
            logger.debug(e.toString());
        }
        return listaJugadores;
    }

    public void cerrarConexion() {
        try {
            con.close();
        } catch (Exception e) {
            logger.debug(e.getMessage());
        }
    }

}
