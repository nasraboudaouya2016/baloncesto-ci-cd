<%@ page import="models.Jugador" %>
<%@ page import="java.util.ArrayList" %>
<%--
  Created by IntelliJ IDEA.
  User: Nasr
  Date: 7/1/2022
  Time: 03:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%
    ArrayList<Jugador> listaJugadores = (ArrayList<Jugador>) request.getSession().getAttribute("jugadores");
%>
<head>
    <title>Tabla de Votos</title>
    <link href="estilos.css" rel="stylesheet" type="text/css" />
</head>
<body>
  <h1>Tabla de Votos</h1>
  <table class="tabla-votos" id="tabla-votos">
    <tr>
        <td>Nombre</td>
        <td>Votos</td>
    </tr>
      <%
          for (Jugador j : listaJugadores) {
      %>
    <tr>
        <td>
            <%=j.getNombre()%>
        </td>
        <td id="nVotos">
            <%=j.getVotos()%>
        </td>
    </tr>
      <%}%>
  </table>
  <br> <a href="index.html"> Ir al comienzo</a>
</body>
</html>
