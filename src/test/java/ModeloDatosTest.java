import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import static org.junit.jupiter.api.Assertions.*;

public class ModeloDatosTest {

    public ModeloDatos instance = new ModeloDatos();

    @Test
    public void testExisteJugador() {
        System.out.println("Prueba de existeJugador");
        instance.abrirConexion();
        String nombre = "";
        boolean expResult = false;
        boolean result = instance.existeJugador(nombre);
        assertEquals(expResult, result);
        //fail("Fallo forzado.");
    }

    @Test
    public void TestActualizarJugador() throws Exception {
        System.out.println("Prueba de actualizarJugador");
        String nombre = "Carroll";
        instance.abrirConexion();
        instance.reinicializarVotos();
        instance.actualizarJugador(nombre);
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/baloncesto", "root", "nasrmysql");;
        Statement set = con.createStatement();
        ResultSet rs = set.executeQuery("SELECT votos FROM Jugadores where nombre " + " LIKE '%" + nombre + "%'");
        String arr = null;
        while (rs.next()) {
            String res = rs.getString("Votos");
            arr = res.replace("\n", ",");
            System.out.println(arr);
        }
        String expResult = "1";
        assertEquals(expResult, arr);
    }

}